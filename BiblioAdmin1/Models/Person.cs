﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("Person")]
    [Index(nameof(CountryId), Name = "fk_PersonCountryId")]
    public partial class Person
    {
        public Person()
        {
            Users = new HashSet<User>();
        }

        [Required]
        [Column(TypeName = "varchar(50)")]
        public string FirstName { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        public string LastName { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Email { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Address1 { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Address2 { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string PostalCode { get; set; }
        [Column(TypeName = "varchar(80)")]
        public string City { get; set; }
        [Column(TypeName = "int(11)")]
        public int? CountryId { get; set; }
        [Column(TypeName = "varchar(25)")]
        public string Phone1 { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Birthday { get; set; }
        [Column(TypeName = "int(11)")]
        public int? Rating { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [ForeignKey(nameof(CountryId))]
        [InverseProperty("People")]
        public virtual Country Country { get; set; }
        [InverseProperty(nameof(User.Person))]
        public virtual ICollection<User> Users { get; set; }
    }
}
