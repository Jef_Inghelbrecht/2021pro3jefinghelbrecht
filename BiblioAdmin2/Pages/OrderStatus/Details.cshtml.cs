using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.OrderStatus
{
    public class DetailsModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public DetailsModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public Bll.OrderStatus OrderStatus { get; set; }
        public void OnGet(int id)
        {
            this.OrderStatus = dbContext.OrderStatuses.SingleOrDefault(m => m.Id == id);
        }

        public void OnGet()
        {
            this.OrderStatus = dbContext.OrderStatuses.SingleOrDefault(m => m.Id == 1);

        }

    }
}
