﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using BiblioAdmin.Bll;

namespace BiblioAdmin.Pages.Biblio
{
    public class CreateModel : PageModel
    {
        private readonly BiblioAdmin.Bll.Docent2Context _context;

        public CreateModel(BiblioAdmin.Bll.Docent2Context context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Bll.OrderStatus OrderStatus { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.OrderStatuses.Add(OrderStatus);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
