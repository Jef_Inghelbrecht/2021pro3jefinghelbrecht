﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.View
{
    class BookConsole
    {
        public Bll.Book Model { get; set; }

        public BookConsole(Bll.Book book)
        {
            Model = book;
        }

        public void List(Bll.Book book)
        {
            Model = book;
        }

        public void List()
        {
            foreach (Bll.Book book in Model.List)
            {
                // One of the most versatile and useful additions to the C# language in version 6
                // is the null conditional operator ?.Post           
                Console.WriteLine("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}\n{7}",
                    book?.Title,
                    book?.Year,
                    book?.City,
                    book?.Publisher,
                    book?.Author,
                    book?.Edition,
                    book?.Translator,
                    book?.Comment);
            }
        }
    }
}
