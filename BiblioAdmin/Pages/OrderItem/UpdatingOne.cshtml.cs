using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.OrderItem
{
    public class UpdatingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;
        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public UpdatingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }

        [BindProperty]
        public Bll.OrderItem OrderItem { get; set; }
        public List<Bll.OrderItem> OrderItemList { get; set; }

        public void OnGet(int? id)
        {
            this.OrderItem = dbContext.OrderItem.SingleOrDefault(m => m.Id == id);
            OrderItemList = dbContext.OrderItem.ToList();
        }
        public ActionResult OnPostUpdate(Bll.OrderItem orderItem)
        {
            if (!ModelState.IsValid)
            {
                // als er een foutief gegeven is ingetypt ga terug
                // de pagina en toon de fout
                return Page(); // return page
            }
            // dbContext.OrderItem.Update(orderItem);
            dbContext.OrderItem.Update(orderItem);
            dbContext.SaveChanges();
            // keer terug naar de index pagina van OrderItem
            return RedirectToPage("Index");
        }
    }
}
