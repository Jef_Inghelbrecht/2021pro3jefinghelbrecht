﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("Event")]
    [Index(nameof(EventCategoryId), Name = "fk_EventEventCategoryId")]
    [Index(nameof(EventTopicId), Name = "fk_EventEventTopicId")]
    public partial class Event
    {
        [Required]
        [Column(TypeName = "varchar(120)")]
        public string Name { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        public string Location { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Starts { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? Ends { get; set; }
        [Required]
        [Column(TypeName = "varchar(255)")]
        public string Image { get; set; }
        [Required]
        [Column(TypeName = "varchar(1024)")]
        public string Description { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        public string OrganiserName { get; set; }
        [Required]
        [Column(TypeName = "varchar(120)")]
        public string OrganiserDescription { get; set; }
        [Column(TypeName = "int(11)")]
        public int? EventCategoryId { get; set; }
        [Column(TypeName = "int(11)")]
        public int? EventTopicId { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [ForeignKey(nameof(EventCategoryId))]
        [InverseProperty("Events")]
        public virtual EventCategory EventCategory { get; set; }
        [ForeignKey(nameof(EventTopicId))]
        [InverseProperty("Events")]
        public virtual EventTopic EventTopic { get; set; }
    }
}
