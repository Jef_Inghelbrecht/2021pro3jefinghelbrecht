﻿using System;

namespace Les6LerenWerkenMetCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            LerenWerkenMetGegevens lerenWerkenMetGegevens = new LerenWerkenMetGegevens();
            Console.WriteLine(lerenWerkenMetGegevens.charLerenGebruiken());
            LerenWerkenMetGegevens.FormatNumericSample();
            int j = 4;
            Console.WriteLine(LerenWerkenMetGegevens.valueVSReference2(ref j));
            Console.WriteLine(LerenWerkenMetGegevens.valueVSReference2(ref j));
            Console.WriteLine($"De waarde van j is {j}");

            Point p1 = new Point(3, 4);
            Console.WriteLine(p1.ToString());
            Point p2 = p1;
            p2.x = 20;
            p2.y = 40;
            Console.WriteLine(p1.ToString());
            Console.WriteLine(p2.ToString());

            MyPoint myP1 = new MyPoint(3, 4);
            Console.WriteLine(myP1.ToString());
            MyPoint myP2 = myP1;
            myP2.x = 20;
            myP2.y = 40;
            Console.WriteLine(myP1.ToString());
            Console.WriteLine(myP2.ToString());


        }
    }
}
