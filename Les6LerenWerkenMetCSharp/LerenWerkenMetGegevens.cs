﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Les6LerenWerkenMetCSharp
{
    public struct Point
    {
        public int x, y;

        public Point(int p1, int p2)
        {
            x = p1;
            y = p2;
        }

        override public string ToString()
        {
            return $"x waarde: {x}, y waarde: {y}";
        }
    }

    public class MyPoint
    {
        public int x, y;

        public MyPoint(int p1, int p2)
        {
            x = p1;
            y = p2;
        }

        override public string ToString()
        {
            return $"x waarde: {x}, y waarde: {y}";
        }
    }

    class LerenWerkenMetGegevens
    {
        public string charLerenGebruiken()
        {
            char ch1 = 'u';
            Char ch2 = new Char();
            ch2 = 'u';
            string resultaat = "u in hoofdletters: " + Char.ToUpper(ch2);
            return $"u in hoofdletter: {Char.ToUpper(ch2)}";
        }

        public static void FormatNumericSample()
        {
            double value = 1.2;
            Console.WriteLine(value.ToString("0.00", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                        "{0:0.00}", value));

            CultureInfo bel = CultureInfo.CreateSpecificCulture("nl-BE");
            Console.WriteLine(value.ToString("0.0", bel));
            Console.WriteLine(String.Format(bel, "{0:0.0}", value));

            CultureInfo nl = CultureInfo.CreateSpecificCulture("nl-NL");
            Console.WriteLine(value.ToString("0.0", nl));
            Console.WriteLine(String.Format(nl, "{0:0.0}", value));

            // Vietnamees geldsymbool
            string sAnswer = value.ToString("c", bel) + "\n";
            Console.WriteLine("Vietnamees geld: {0}", sAnswer);

        }

        public static void valueVsReference()
        {
            int i = 4;
            int y = i;
            y = 5;
            Console.WriteLine($"De waarde van i is: {i}");
        }

        public static int valueVSReference2(ref int i)
        {
            i = 10;
            return i;
        }
   

    }
}
