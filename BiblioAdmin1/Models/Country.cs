﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("Country")]
    [Index(nameof(Code), Name = "uc_Country_Code", IsUnique = true)]
    [Index(nameof(Name), Name = "uc_Country_Name", IsUnique = true)]
    public partial class Country
    {
        public Country()
        {
            People = new HashSet<Person>();
        }

        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(2)")]
        public string Code { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [InverseProperty(nameof(Person.Country))]
        public virtual ICollection<Person> People { get; set; }
    }
}
