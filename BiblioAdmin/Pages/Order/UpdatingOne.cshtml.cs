using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.Order
{
    public class UpdatingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;
        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public UpdatingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }

        [BindProperty]
        public Bll.Order Order { get; set; }
        public List<Bll.Order> OrderList { get; set; }
        public List<Bll.Customer> CustomerList { get; set; }
        public List<Bll.OrderStatus> OrderStatusList { get; set; }
        public List<Bll.ShippingMethod> ShippingMethodList { get; set; }

        public void OnGet(int? id)
        {
            this.Order = dbContext.Order.SingleOrDefault(m => m.Id == id);
            Order.ShippingMethod = dbContext.ShippingMethod.SingleOrDefault(m => m.Id == this.Order.ShippingMethodId);
            Order.Status = dbContext.OrderStatus.SingleOrDefault(m => m.Id == this.Order.StatusId);
            Order.Customer = dbContext.Customer.SingleOrDefault(m => m.Id == this.Order.CustomerId);
            OrderList = dbContext.Order.ToList();
            CustomerList = dbContext.Customer.ToList();
            OrderStatusList = dbContext.OrderStatus.ToList();
            ShippingMethodList = dbContext.ShippingMethod.ToList();
        }
        public ActionResult OnPostUpdate(Bll.Order order)
        {
            if (!ModelState.IsValid)
            {
                // als er een foutief gegeven is ingetypt ga terug
                // de pagina en toon de fout
                return Page(); // return page
            }
            // dbContext.OrderStatus.Update(orderStatus);
            dbContext.Order.Update(order);
            dbContext.SaveChanges();
            // keer terug naar de index pagina van OrderStatus
            return RedirectToPage("Index");
        }
    }
}
