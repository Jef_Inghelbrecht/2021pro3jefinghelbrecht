﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BiblioAdmin.Bll;

namespace BiblioAdmin.Pages.Biblio
{
    public class DeleteModel : PageModel
    {
        private readonly BiblioAdmin.Bll.Docent2Context _context;

        public DeleteModel(BiblioAdmin.Bll.Docent2Context context)
        {
            _context = context;
        }

        [BindProperty]
        public Bll.OrderStatus OrderStatus { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrderStatus = await _context.OrderStatuses.FirstOrDefaultAsync(m => m.Id == id);

            if (OrderStatus == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrderStatus = await _context.OrderStatuses.FindAsync(id);

            if (OrderStatus != null)
            {
                _context.OrderStatuses.Remove(OrderStatus);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
