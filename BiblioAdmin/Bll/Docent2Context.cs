﻿using Microsoft.EntityFrameworkCore;

namespace BiblioAdmin.Bll
{
    public partial class Docent2Context : DbContext
    {
        public Docent2Context()
        {
        }

        public Docent2Context(DbContextOptions<Docent2Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<ShippingMethod> ShippingMethod { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

    }
}
