using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.Order
{
    public class IndexModel : PageModel
    {
        public List<Bll.Order> OrderList { get; set; }

        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public IndexModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public void OnGet()
        {
            OrderList = dbContext.Order
                .Join(dbContext.Customer,
                      p => p.CustomerId,
                      e => e.Id,
                      (p, e) => new Bll.Order()
                      {
                          CustomerName = e.FirstName + " " + e.LastName,
                          Id = p.Id,
                          StatusId = p.StatusId,
                          ShippingMethodId = p.ShippingMethodId,
                          Comment = p.Comment,
                          ShippingDate = p.ShippingDate
                      }
                      ).ToList();

            // OrderList = dbContext.Order.ToList();

            OrderList = (from order in dbContext.Order
                         join customer in dbContext.Customer on order.CustomerId equals customer.Id
                         select new Bll.Order()
                         {
                             CustomerName = customer.FirstName + " " + customer.LastName,
                             Id = order.Id,
                             StatusId = order.StatusId,
                             ShippingMethodId = order.ShippingMethodId,
                             Comment = order.Comment,
                             ShippingDate = order.ShippingDate
                         }
                      ).ToList();

        }
    }
}
