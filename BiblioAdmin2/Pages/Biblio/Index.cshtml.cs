﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BiblioAdmin.Bll;

namespace BiblioAdmin.Pages.Biblio
{
    public class IndexModel : PageModel
    {
        private readonly BiblioAdmin.Bll.Docent2Context _context;

        public IndexModel(BiblioAdmin.Bll.Docent2Context context)
        {
            _context = context;
        }

        public IList<Bll.OrderStatus> OrderStatus { get;set; }

        public async Task OnGetAsync()
        {
            OrderStatus = await _context.OrderStatuses.ToListAsync();
        }
    }
}
