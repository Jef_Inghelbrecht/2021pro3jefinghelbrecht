﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("Role")]
    [Index(nameof(Name), Name = "uc_Role_Name", IsUnique = true)]
    public partial class Role
    {
        public Role()
        {
            Users = new HashSet<User>();
        }

        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [InverseProperty(nameof(User.Role))]
        public virtual ICollection<User> Users { get; set; }
    }
}
