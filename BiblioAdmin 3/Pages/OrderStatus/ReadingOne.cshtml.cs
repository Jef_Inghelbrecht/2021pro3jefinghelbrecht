using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.OrderStatus
{
    public class ReadingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public ReadingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }

        public Bll.OrderStatus OrderStatus { get; set; }
        public List<Bll.OrderStatus> OrderStatusList { get; set; }

        public void OnGet(int? id)
        {
            this.OrderStatus = dbContext.OrderStatus.SingleOrDefault(m => m.Id == id);
            OrderStatusList = dbContext.OrderStatus.ToList();
        }
    }
}
