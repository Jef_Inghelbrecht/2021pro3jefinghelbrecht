﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    public partial class MmtLike
    {
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(5)")]
        public string Key { get; set; }
        [Column(TypeName = "int(11)")]
        public int? Likes { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
    }
}
