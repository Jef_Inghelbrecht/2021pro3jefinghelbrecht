﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("EventTopic")]
    [Index(nameof(Name), Name = "uc_EventTopic_Name", IsUnique = true)]
    public partial class EventTopic
    {
        public EventTopic()
        {
            Events = new HashSet<Event>();
        }

        [Required]
        [Column(TypeName = "varchar(120)")]
        public string Name { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [InverseProperty(nameof(Event.EventTopic))]
        public virtual ICollection<Event> Events { get; set; }
    }
}
