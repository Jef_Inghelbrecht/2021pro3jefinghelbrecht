﻿using System;

namespace BookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TryOutCsv();
            TryOutXml();
            TryOutJson();
            Console.ReadKey();
        }

        static void TryOutCsv()
        {
            Console.WriteLine("De Book App CSV");
            Bll.Book book = new Bll.Book();
            Dal.BookCsv bookCsv = new Dal.BookCsv(book);
            // de seperator staat standaard op ;
            // in het Book.csv bestand is dat |
            bookCsv.Separator = '\n';
            bookCsv.Book = book;
            bookCsv.ReadAll();
            Console.WriteLine(bookCsv.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            // serialize boeken met een andere separator
            // naar ander bestand
            bookCsv.ConnectionString = "Data/Book2";
            bookCsv.Create(';');
            Console.WriteLine(bookCsv.Message);
            
        }

        static void TryOutXml()
        {
            Console.WriteLine("De Book App XML");
            Bll.Book book = new Bll.Book();
            Dal.BookXml bookXml = new Dal.BookXml(book);
            bookXml.Book = book;
            bookXml.ReadAll();
            Console.WriteLine(bookXml.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            // serialize naar ander bestand
            bookXml.ConnectionString = "Data/Book2";
            bookXml.Create();
            Console.WriteLine(bookXml.Message);
            
        }

        static void TryOutJson()
        {
            Console.WriteLine("De Postcode App Json");
            Bll.Book book = new Bll.Book();
            Dal.BookJson bookJson = new Dal.BookJson(book);
            bookJson.Book = book;
            bookJson.ReadAll();
            Console.WriteLine(bookJson.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            // serialize naar ander bestand
            bookJson.ConnectionString = "Data/Book2";
            bookJson.Create();
            Console.WriteLine(bookJson.Message);
        }

        
    }
}
