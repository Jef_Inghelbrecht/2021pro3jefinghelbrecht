﻿using DotNetCore.Learning;
using System;
using System.Collections.Generic;

namespace DontNetCore.Learning
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Gegevensserialisatie");
            List<Book> books = BookSerializing.DeserializeFromXml();
            BookSerializing.ShowBooks(books);
            BookSerializing.SerializeListToCsv(books, @"Data/Book.csv", "|");
            books = BookSerializing.DeserializeCsvToList("|");
            Console.WriteLine("".PadLeft(60, '*'));
            Console.WriteLine("\n\n\nIngelezen CSV bestand!\n\n\n");
            Console.WriteLine("".PadLeft(60, '*'));
            BookSerializing.ShowBooks(books);
            BookSerializing.SerializeListToJson(books, @"Data/Book.json");
            BookSerializing.DeserializeJsonToList(@"Data/Book.json");
            Console.WriteLine("".PadLeft(60, '*'));
            Console.WriteLine("\n\n\nIngelezen JSON bestand!\n\n\n");
            Console.WriteLine("".PadLeft(60, '*'));
            BookSerializing.ShowBooks(books);



        }

        static void Probeersels()
        {
            List<Book> books = BookSerializing.DeserializeFromXml();
            BookSerializing.ShowBooks(books);
            TryOut.SerializeListToCsv(books, @"Data/Book.csv", "|");
            books = TryOut.DeserializeCsvToList("|");
            Console.WriteLine("".PadLeft(60, '*'));
            Console.WriteLine("\n\n\nIngelezen van CSV bestand!\n\n\n");
            Console.WriteLine("".PadLeft(60, '*'));
            BookSerializing.ShowBooks(books);
            TryOut.SerializeListToJson(books, @"Data/Book.json");
            books = TryOut.DeserializeJsonToList(@"Data/Book.json");
            Console.WriteLine("".PadLeft(60, '*'));
            Console.WriteLine("\n\n\nIngelezen van JSON bestand!\n\n\n");
            Console.WriteLine("".PadLeft(60, '*'));
            BookSerializing.ShowBooks(books);
        }
    }
}
