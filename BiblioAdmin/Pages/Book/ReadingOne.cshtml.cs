using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.Book
{
    public class ReadingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public ReadingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public Bll.Book Book { get; set; }
        public List<Bll.Book> BookList { get; set; }


        public void OnGet(int? id)
        {
            this.Book = dbContext.Book.SingleOrDefault(m => m.Id == id);
            BookList = dbContext.Book.ToList();
        }
        public ActionResult OnGetDelete(int? id)
        {
            if (id != null)
            {

                Bll.Book book = new Bll.Book();
                book.Id = (int)id;
                dbContext.Remove(book);
                dbContext.SaveChanges();
                return RedirectToPage("Index");
            }
            return Page();
        }
    }
    }

