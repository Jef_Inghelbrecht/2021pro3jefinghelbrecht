using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.ShippingMethod
{
    public class IndexModel : PageModel
    {
        public List<Bll.ShippingMethod> ShippingMethodList { get; set; }

        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public IndexModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public void OnGet()
        {
            ShippingMethodList = dbContext.ShippingMethod.ToList();
        }
    }
}
