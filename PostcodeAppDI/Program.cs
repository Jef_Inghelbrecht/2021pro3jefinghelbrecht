﻿using Microsoft.Extensions.DependencyInjection;
using PostcodeApp.Dal;
using System;

namespace PostcodeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            serviceProvider.GetService<App>().TryOut();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<PostcodeApp.Dal.IPostcode>
                (p => new PostcodeApp.Dal.PostcodeXml(new PostcodeApp.Bll.Postcode()));
            serviceCollection.AddTransient<App>();
        }
    }
}
