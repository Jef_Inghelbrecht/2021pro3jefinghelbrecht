﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCore.Learning
{
    public class Book
    {
        private string title;

        public string Title
        {
            get { return title; }
            set
            {
                string text = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart();
                title = text.Replace("  ", " ");

            }
        }

        private string year;

        public string Year
        {
            get { return year; }
            set { year = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart(); }
        }

        private string city;

        public string City
        {
            get { return city; }
            set { city = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart(); }
        }

        private string publisher;

        public string Publisher
        {
            get { return publisher; }
            set { publisher = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart(); }
        }

        private string author;

        public string Author
        {
            get { return author; }
            set
            {
                author = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart();
            }
        }

        private string edition;

        public string Edition
        {
            get { return edition; }
            set { edition = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart(); }
        }

        private string translator;

        public string Translator
        {
            get { return translator; }
            set { translator = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart(); }
        }

        private string comment;

        public string Comment
        {
            get { return comment; }
            set { comment = value.Replace("\n", string.Empty).Replace("  ", " ").TrimStart(); }
        }

        private List<Book> list;

        public List<Book> List
        {
            get { return list; }
            set { list = value; }
        }


    }
}
