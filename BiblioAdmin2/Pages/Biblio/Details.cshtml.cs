﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BiblioAdmin.Bll;

namespace BiblioAdmin.Pages.Biblio
{
    public class DetailsModel : PageModel
    {
        private readonly BiblioAdmin.Bll.Docent2Context _context;

        public DetailsModel(BiblioAdmin.Bll.Docent2Context context)
        {
            _context = context;
        }

        public Bll.OrderStatus OrderStatus { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrderStatus = await _context.OrderStatuses.FirstOrDefaultAsync(m => m.Id == id);

            if (OrderStatus == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
