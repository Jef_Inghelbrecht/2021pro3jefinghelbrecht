﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace BookApp.Dal
{
    class BookJson : IBook
    {
        // Een Book BLL object om de opgehaalde waarden
        // in op te slagen
        public Bll.Book Book { get; set; }
        // Error message
        public string Message { get; set; }
        public string FileName { get; set; }
        private string connectionString = @"Data/Book";
        public string ConnectionString
        {
            get
            {
                return connectionString + ".xml";
            }
            set
            {
                connectionString = value;
            }
        }
        public BookJson(Bll.Book book)
        {
            Book = book;
        }

        // een overload om de naam van het csv bestand in te stellen
        public BookJson(string connectionString)
        {
            ConnectionString = connectionString;
        }

        /// <summary>
        /// In het geval van JSON wordt heel de List gesaved
        /// </summary>
        /// <returns></returns>
        public bool Create()
        {
            try
            {
                TextWriter writer = new StreamWriter(ConnectionString);
                // static method SerilizeObject van Newtonsoft.Json
                string bookString = Newtonsoft.Json.JsonConvert.SerializeObject(Book.List);
                writer.WriteLine(bookString);
                writer.Close();
                Message = $"Het bestand met de naam {ConnectionString} is met succes geserialiseerd.";
                return true;

                //TextWriter writer = new StreamWriter(ConnectionString);
                //// static method SerilizeObject van Newtonsoft.Json
                //string postcodeString = Newtonsoft.Json.JsonConvert.SerializeObject(Postcode.List);
                //writer.WriteLine(postcodeString);
                //writer.Close();
                //Message = $"Het bestand met de naam {ConnectionString} is met succes geserialiseerd.";
                //return true;
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                Message = $"Kan het bestand met de naam {ConnectionString} niet serialiseren.\nFoutmelding {e.Message}.";
                return false;
            }


        }

        public bool ReadAll()
        {

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Bll.Book[]));
                StreamReader file = new System.IO.StreamReader(ConnectionString);
                Bll.Book[] books = (Bll.Book[])serializer.Deserialize(file);
                file.Close();
                // array converteren naar List
                Book.List = new List<Bll.Book>(books);
                Message = $"Bestand {ConnectionString} is met succes gedeserialiseerd.";
                return true;
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                Message = $"Het bestand {ConnectionString} s niet gedeserialiseerd.\nFoutmelding {e.Message}.";
                return false;
            }

        }
    }
}
