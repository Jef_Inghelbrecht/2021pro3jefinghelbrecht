using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.OrderStatus
{
    public class IndexModel : PageModel
    {
        public List<Bll.OrderStatus> OrderStatusList { get; set; }
        private readonly Bll.Docent2Context dbContext;

        public IndexModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }


        public void OnGet()
        {
            OrderStatusList = dbContext.OrderStatus.ToList();
        }
    }
}
