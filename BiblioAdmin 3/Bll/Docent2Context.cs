﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BiblioAdmin.Bll
{
    public partial class Docent2Context : DbContext
    {
        public Docent2Context()
        {
        }

        public Docent2Context(DbContextOptions<Docent2Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<ShippingMethod> ShippingMethod { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=51.38.37.150;user id=Docent2;password=Docent_63U1T2R3;port=3306;database=Docent2;sslmode=none", x => x.ServerVersion("5.7.32-mysql"));
            }
        }

 
    }
}
