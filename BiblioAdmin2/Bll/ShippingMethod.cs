﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Bll
{
    [Table("ShippingMethod")]
    public partial class ShippingMethod
    {
        [Required]
        [Column(TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Required]
        [Column(TypeName = "varchar(1024)")]
        public string Description { get; set; }
        public decimal? Price { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
    }
}
