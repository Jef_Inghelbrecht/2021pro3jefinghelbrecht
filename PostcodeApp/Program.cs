﻿using System;

namespace PostcodeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            TryOutCsv();
        }

        static void TryOutCsv()
        {
            Console.WriteLine("De Postcode App CSV");
            Bll.Postcode postcode = new Bll.Postcode();
            Dal.PostcodeCsv postcodeCsv = new Dal.PostcodeCsv(postcode);
            // de seperator staat standaard op ;
            // in het Postcode.csv bestand is dat |
            postcodeCsv.Separator = ';';
            postcodeCsv.Postcode = postcode;
            postcodeCsv.ReadAll();
            Console.WriteLine(postcodeCsv.Message);
            View.PostcodeConsole view = new View.PostcodeConsole(postcode);
            view.List();
            // serialize postcodes met een andere separator
            // naar ander bestand
            postcodeCsv.ConnectionString = "Data/Postcode2";
            postcodeCsv.Create(';');
            Console.WriteLine(postcodeCsv.Message);
        }
    }
}
