using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.Customer
{
    public class ReadingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public ReadingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public Bll.Customer Customer { get; set; }
        public List<Bll.Customer> CustomerList { get; set; }


        public void OnGet(int? id)
        {
            this.Customer = dbContext.Customer.SingleOrDefault(m => m.Id == id);
            CustomerList = dbContext.Customer.ToList();
        }
        public ActionResult OnGetDelete(int? id)
        {
            if (id != null)
            {

                Bll.Customer customer = new Bll.Customer();
                customer.Id = (int)id;
                dbContext.Remove(customer);
                dbContext.SaveChanges();
                return RedirectToPage("Index");
            }
            return Page();
        }
    }
}
