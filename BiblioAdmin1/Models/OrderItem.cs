﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Keyless]
    [Table("OrderItem")]
    public partial class OrderItem
    {
        [Column(TypeName = "int(11)")]
        public int BookId { get; set; }
        [Column(TypeName = "int(11)")]
        public int OrderId { get; set; }
        public decimal? Quantity { get; set; }
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }
    }
}
