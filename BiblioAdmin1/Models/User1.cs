﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("Users")]
    public partial class User1
    {
        [Key]
        [Column(TypeName = "int(11) unsigned")]
        public uint Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(30)")]
        public string FirstName { get; set; }
        [Required]
        [Column(TypeName = "varchar(30)")]
        public string LastName { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Email { get; set; }
        [Column(TypeName = "int(3)")]
        public int? Age { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Location { get; set; }
        [Column(TypeName = "timestamp")]
        public DateTime Date { get; set; }
    }
}
