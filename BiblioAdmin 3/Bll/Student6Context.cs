﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BiblioAdmin.Bll
{
    public partial class Student6Context : DbContext
    {
        public Student6Context()
        {
        }

        public Student6Context(DbContextOptions<Student6Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<ShippingMethod> ShippingMethod { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=51.38.37.150;user id=Student6;password=Student$B4JV4BVY;port=3306;database=Student6;sslmode=none", x => x.ServerVersion("5.7.32-mysql"));
            }
        }

 

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
