using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.OrderItem
{
    public class ReadingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public ReadingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public Bll.OrderItem OrderItem { get; set; }
        public List<Bll.OrderItem> OrderItemList { get; set; }


        public void OnGet(int? id)
        {
            this.OrderItem = dbContext.OrderItem.SingleOrDefault(m => m.Id == id);
            OrderItemList = dbContext.OrderItem.ToList();
        }
        public ActionResult OnGetDelete(int? id)
        {
            if (id != null)
            {

                Bll.OrderItem orderItem = new Bll.OrderItem();
                orderItem.Id = (int)id;
                dbContext.Remove(orderItem);
                dbContext.SaveChanges();
                return RedirectToPage("Index");
            }
            return Page();
        }
     }
}

