using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.Order
{
    public class ReadingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public ReadingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public Bll.Order Order { get; set; }
        public Bll.OrderItem OrderItem { get; set; }
        public List<Bll.Order> OrderList { get; set; }
        public List<Bll.OrderItem> OrderItemList { get; set; }


        public void OnGet(int? id)
        {
            this.Order = dbContext.Order.SingleOrDefault(m => m.Id == id);
            Order.ShippingMethod = dbContext.ShippingMethod.SingleOrDefault(m => m.Id == this.Order.ShippingMethodId);
            Order.Status = dbContext.OrderStatus.SingleOrDefault(m => m.Id == this.Order.StatusId);
            Order.Customer = dbContext.Customer.SingleOrDefault(m => m.Id == this.Order.CustomerId);
            OrderList = dbContext.Order.ToList();
            OrderItemList = dbContext.OrderItem.ToList();
        }
        public ActionResult OnGetDelete(int? id)
        {
            if (id != null)
            {

                Bll.Order order = new Bll.Order();
                order.Id = (int)id;
                dbContext.Remove(order);
                dbContext.SaveChanges();
                return RedirectToPage("Index");
            }
            return Page();
        }

        public ActionResult OnPostInsertOrderItem(Bll.OrderItem orderItem)
        {
            if (!ModelState.IsValid)
            {
                // als er een foutief gegeven is ingetypt ga terug
                // de pagina en toon de fout
                return Page(); // return page, nog een nieuwe ingeven
            }
            // dbContext.OrderItem.Update(orderItem);
            dbContext.OrderItem.Add(orderItem);
            dbContext.SaveChanges();
            // keer terug naar de ReadingOne pagina van Order
            return RedirectToPage("ReadingOne");
        }

        public ActionResult OnPostDeleteOrderItem(Bll.OrderItem orderItem)
        {
                 dbContext.OrderItem.Remove(orderItem);
                dbContext.SaveChanges();
                return RedirectToPage("ReadingOne");
  
        }
    }
}
