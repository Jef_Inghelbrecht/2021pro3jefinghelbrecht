﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("migrations")]
    public partial class Migration
    {
        [Key]
        [Column("id", TypeName = "int(10) unsigned")]
        public uint Id { get; set; }
        [Required]
        [Column("migration", TypeName = "varchar(255)")]
        public string Migration1 { get; set; }
        [Column("batch", TypeName = "int(11)")]
        public int Batch { get; set; }
    }
}
