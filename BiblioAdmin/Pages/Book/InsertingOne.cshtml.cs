using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.Book
{
    public class InsertingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;
        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public InsertingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }

        [BindProperty]
        public Bll.Book Book { get; set; }
        public List<Bll.Book> BookList { get; set; }
        public void OnGet()
        {
            BookList = dbContext.Book.ToList();
        }

        public ActionResult OnPostInsert(Bll.Book book)
        {
            if (!ModelState.IsValid)
            {
                // als er een foutief gegeven is ingetypt ga terug
                // de pagina en toon de fout
                return Page(); // return page, nog een nieuwe ingeven
            }
            // dbContext.Book.Update(book);
            dbContext.Book.Add(book);
            dbContext.SaveChanges();
            // keer terug naar de index pagina van Book
            return RedirectToPage("Index");
        }
    }
}
