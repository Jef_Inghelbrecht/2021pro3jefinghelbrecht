﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BiblioAdmin.Models
{
    [Table("User")]
    [Index(nameof(PersonId), Name = "fk_UserPersonId")]
    [Index(nameof(RoleId), Name = "fk_UserRoleId")]
    [Index(nameof(Name), Name = "uc_User_Name", IsUnique = true)]
    public partial class User
    {
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(255)")]
        public string Salt { get; set; }
        [Required]
        [Column(TypeName = "varchar(255)")]
        public string HashedPassword { get; set; }
        [Column(TypeName = "int(11)")]
        public int? PersonId { get; set; }
        [Column(TypeName = "int(11)")]
        public int? RoleId { get; set; }
        [Key]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [ForeignKey(nameof(PersonId))]
        [InverseProperty("Users")]
        public virtual Person Person { get; set; }
        [ForeignKey(nameof(RoleId))]
        [InverseProperty("Users")]
        public virtual Role Role { get; set; }
    }
}
