using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.ShippingMethod
{
    public class InsertingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;
        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public InsertingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }

        [BindProperty]
        public Bll.ShippingMethod ShippingMethod { get; set; }
        public List<Bll.ShippingMethod> ShippingMethodList { get; set; }
        public void OnGet()
        {
            ShippingMethodList = dbContext.ShippingMethod.ToList();
        }

        public ActionResult OnPostInsert(Bll.ShippingMethod shippingmethod)
        {
            if (!ModelState.IsValid)
            {
                // als er een foutief gegeven is ingetypt ga terug
                // de pagina en toon de fout
                return Page(); // return page, nog een nieuwe ingeven
            }
            // dbContext.ShippingMethod.Update(shippingmethod);
            dbContext.ShippingMethod.Add(shippingmethod);
            dbContext.SaveChanges();
            // keer terug naar de index pagina van ShippingMethod
            return RedirectToPage("Index");
        }
    }
}
