using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.OrderStatus
{
    public class IndexModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;
        public List<Bll.OrderStatus> OrderStatusList { get; set; }
            
        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public IndexModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }

        public void OnGet()
        {
            OrderStatusList = dbContext.OrderStatuses.ToList();
        }
    }
}
