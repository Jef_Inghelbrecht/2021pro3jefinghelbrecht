using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BiblioAdmin.Pages.ShippingMethod
{
    public class ReadingOneModel : PageModel
    {
        private readonly Bll.Docent2Context dbContext;

        // voeg constructor toe om ge�njecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public ReadingOneModel(Bll.Docent2Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public Bll.ShippingMethod ShippingMethod { get; set; }
        public List<Bll.ShippingMethod> ShippingMethodList { get; set; }


        public void OnGet(int? id)
        {
            this.ShippingMethod = dbContext.ShippingMethod.SingleOrDefault(m => m.Id == id);
            ShippingMethodList = dbContext.ShippingMethod.ToList();
        }
        public ActionResult OnGetDelete(int? id)
        {
            if (id != null)
            {

                Bll.ShippingMethod shippingmethod = new Bll.ShippingMethod();
                shippingmethod.Id = (int)id;
                dbContext.Remove(shippingmethod);
                dbContext.SaveChanges();
                return RedirectToPage("Index");
            }
            return Page();
        }
    }
}
